<ul class="nav nav-list" style="background-color: rgb(34, 42, 45);">
    <li class="active">
        <a href="index.html">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> 菜单 </span>
        </a>

        <b class="arrow"></b>
    </li>

<#assign user=request.getSession().getAttribute("user")>
<#if user.level = 0>
<#--系统管理员可见-->
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								系统管理
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="">
                <a href="department.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    部门管理
                </a>

                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="employee.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    员工管理
                </a>

                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="signinout.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    打卡记录管理
                </a>

                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="leaves.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    请假管理
                </a>

                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="field.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    外出申请管理
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
<#else>
<#--各类员工自己的打卡、请假，外出，个人信息的管理,通用-->

    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-user"></i>
							<span class="menu-text">
								个人信息
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="">
                <a href="profile.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    个人信息
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>

<#--打卡记录是从外部通过打卡机引入，因此只能查看，并且能查看全部的打卡记录-->
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-list"></i>
							<span class="menu-text">
								打卡记录
							</span>
            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="">
                <a href="signinoutRecord.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    查看打卡记录
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>

<#--请假记录，员工可以查看所有的请假记录，并提出，修改，删除自己的请假申请-->
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-home"></i>
							<span class="menu-text">
								请假
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="">
                <a href="leavesRecord.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    查看所有
                </a>

                <b class="arrow"></b>
            </li>
            <#if user.level != 3>
                <li class="">
                    <a href="personalLeaves.html" target="content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        个人请假管理
                    </a>

                    <b class="arrow"></b>
                </li>
            </#if>
        </ul>
    </li>

<#--外出申请记录，员工可以查看所有的外出记录，并提出，修改，删除自己的外出申请-->
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-money"></i>
							<span class="menu-text">
								外出
							</span>
            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="">
                <a href="fieldRecord.html" target="content">
                    <i class="menu-icon fa fa-caret-right"></i>
                    查看所有
                </a>

                <b class="arrow"></b>
            </li>
            <#if user.level != 3>
                <li class="">
                    <a href="personalField.html" target="content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        个人外出管理
                    </a>

                    <b class="arrow"></b>
                </li>
            </#if>
        </ul>
    </li>


<#--如果level=2 or 3，则该员工为部门经理或总经理，需要有审批项目-->
    <#if user.level = 2 || user.level = 3>
        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-check"></i>
							<span class="menu-text">
								审批项目
							</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="checkLeaves.html" target="content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        待审请假
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="checkField.html" target="content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        待审外出申请
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
    </#if>

<#--如果department_id = 0，则该员工为行政部，可以导出考勤报表-->
    <#if user.departmentId = 1>

    <#--打卡记录是从外部通过打卡机引入，因此只能查看，并且能查看全部的打卡记录-->
        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
							<span class="menu-text">
								出勤情况
							</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="importSig.html" target="content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        上传出勤记录
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="signinout_summary.html" target="content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        查看出勤记录
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>
    </#if>
</#if>
</ul><!-- /.nav-list -->