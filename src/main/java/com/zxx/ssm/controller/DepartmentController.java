package com.zxx.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.zxx.ssm.dao.Department;
import com.zxx.ssm.dao.DepartmentExample;
import com.zxx.ssm.exception.AbstractServiceRuntimeException;
import com.zxx.ssm.model.enums.ResultCode;
import com.zxx.ssm.model.result.BizResponse;
import com.zxx.ssm.service.DepartmentService;
import com.zxx.ssm.util.PaginationUtil;
import com.zxx.ssm.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.util.List;

/**
 * Created by zxx on 2016/12/8.
 */
@RestController
@RequestMapping("department")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public BizResponse getEmployeeList(@ModelAttribute Department department, @RequestParam(defaultValue = "1") int pageNumber, @RequestParam(defaultValue = "10") int pageSize,
                                       @RequestParam(defaultValue = "id") String sort, @RequestParam(defaultValue = "desc") String order) {
        PaginationUtil.preset(pageNumber, pageSize, sort, order);
        DepartmentExample departmentExample = new DepartmentExample();
        if (StringUtils.isNotBlank(department.getName()))
            departmentExample.createCriteria().andNameLike("%" + department.getName() + "%");
        List<Department> departmentList = departmentService.selectByExample(departmentExample);
        PageInfo pageInfo = new PageInfo(departmentList);
        BizResponse bizResponse = new BizResponse();
        bizResponse.put("department", pageInfo);
        return bizResponse;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse addDepartment(@ModelAttribute Department department) {
        if (StringUtils.isNotBlank(department.getName())) {
            DepartmentExample departmentExample = new DepartmentExample();
            departmentExample.createCriteria().andNameEqualTo(department.getName());
            List<Department> departmentList = departmentService.selectByExample(departmentExample);
            if (departmentList.size() > 0)
                return new BizResponse(ResultCode.DEPARTMENT_ALREADY_EXIST);
            else {
                departmentService.insertSelective(department);
                return new BizResponse();
            }
        } else {
            return new BizResponse(ResultCode.DEPARTMENT_TYPE_ADD_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public BizResponse updateDepartment(@ModelAttribute Department department){
        DepartmentExample departmentExample = new DepartmentExample();
        departmentExample.createCriteria().andIdEqualTo(department.getId());
        if(departmentService.updateByExample(department, departmentExample)>0)
            return new BizResponse();
        else
            return new BizResponse(ResultCode.DEPARTMENT_TYPE_UPDATE_ERROR);
    }

    @RequestMapping(value = "/{strid}", method = RequestMethod.DELETE)
    @ResponseBody
    public BizResponse removeDepartment(@PathVariable("strid") String strid) {
        if (strid != null) {
            String[] idList = strid.split(",");
            for (int i = 0; i < idList.length; i++) {
                try {
                    Integer id = Integer.parseInt(idList[i]);
                    if (id != null)
                        departmentService.delete(id);
                } catch (AbstractServiceRuntimeException e) {
                    e.printStackTrace();
                    return new BizResponse(ResultCode.DEPARTMENT_TYPE_DELETE_ERROR);
                }
            }
        }
        return new BizResponse();
    }
}
