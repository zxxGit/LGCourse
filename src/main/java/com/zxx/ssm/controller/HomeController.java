package com.zxx.ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zxx on 2016/11/23.
 */
@Controller
@RequestMapping("home")
public class HomeController {

    @RequestMapping(value = "login")
    public String login(){
        return "login";
    }

    @RequestMapping(value = "{name}", method = RequestMethod.GET)
    public String render(@PathVariable("name") String name, ModelMap modelMap) {
        modelMap.addAttribute("viewName", name);
        return name;
    }

}
