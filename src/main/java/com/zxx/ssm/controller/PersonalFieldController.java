package com.zxx.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.zxx.ssm.dao.Employee;
import com.zxx.ssm.dao.EmployeeExample;
import com.zxx.ssm.dao.Field;
import com.zxx.ssm.dao.FieldExample;
import com.zxx.ssm.exception.AbstractServiceRuntimeException;
import com.zxx.ssm.model.biz.FieldBiz;
import com.zxx.ssm.model.convertor.FieldConvertor;
import com.zxx.ssm.model.enums.ResultCode;
import com.zxx.ssm.model.form.FieldForm;
import com.zxx.ssm.model.result.BizResponse;
import com.zxx.ssm.service.EmployeeService;
import com.zxx.ssm.service.FieldService;
import com.zxx.ssm.util.DateUtils;
import com.zxx.ssm.util.PaginationUtil;
import com.zxx.ssm.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by bwju on 2016/12/15.
 */
@RestController
@RequestMapping("personalfields")
public class PersonalFieldController {
    @Autowired
    private FieldService fieldService;

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public BizResponse getAll(@ModelAttribute FieldForm fieldForm, @RequestParam(defaultValue = "1") int pageNumber, @RequestParam(defaultValue = "10") int pageSize,
                              @RequestParam(defaultValue = "id") String sort, @RequestParam(defaultValue = "desc") String order, HttpServletRequest request) {
        PaginationUtil.preset(pageNumber, pageSize, sort, order);
        HttpSession session = request.getSession();
        Employee curEmployee = (Employee) session.getAttribute("user");
        fieldForm.setEmployeeName(curEmployee.getName());
        fieldForm.setStartTime(DateUtils.convertString(fieldForm.getStartTime()));
        fieldForm.setEndTime(DateUtils.convertString(fieldForm.getEndTime()));
        List<FieldBiz> fieldsBizList = fieldService.getAll(fieldForm);
        PageInfo pageInfo = new PageInfo(fieldsBizList);
        BizResponse bizResponse = new BizResponse();
        bizResponse.put("fields", pageInfo);
        return bizResponse;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse add(@ModelAttribute FieldForm fieldForm, HttpServletRequest request) {
        try {
                HttpSession session = request.getSession();
                Employee curEmployee = (Employee) session.getAttribute("user");
                fieldForm.setEmployeeId(curEmployee.getId());
                Field theadd = FieldConvertor.convertFieldForm(fieldForm);
                fieldService.insertSelective(theadd);
                return new BizResponse();
        }catch (Exception e){
            return new BizResponse(false,ResultCode.FIELD_TYPE_ADD_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public BizResponse remove( @PathVariable("id") String id) throws AbstractServiceRuntimeException {
        try {
            if (id != null) {
                fieldService.delete(StringUtils.toInt(id));
                return new BizResponse();
            }
            return new BizResponse(false,ResultCode.FIELD_TYPE_DELETE_ERROR);
        }catch (Exception e){
            return new BizResponse(false,ResultCode.FIELD_TYPE_DELETE_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public BizResponse update(@ModelAttribute FieldForm fieldForm, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Employee curEmployee = (Employee) session.getAttribute("user");
        fieldForm.setEmployeeId(curEmployee.getId());
        Field field = FieldConvertor.convertFieldForm(fieldForm);
        field.setState(1);
        FieldExample fieldExample = new FieldExample();
        fieldExample.createCriteria().andIdEqualTo(field.getId());
        if (fieldService.updateByExample(field, fieldExample) > 0)
            return new BizResponse();
        else
            return new BizResponse(ResultCode.FIELD_TYPE_UPDATE_ERROR);
    }
}