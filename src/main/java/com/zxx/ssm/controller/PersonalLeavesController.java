package com.zxx.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.zxx.ssm.dao.Employee;
import com.zxx.ssm.dao.Leaves;
import com.zxx.ssm.dao.LeavesExample;
import com.zxx.ssm.exception.AbstractServiceRuntimeException;
import com.zxx.ssm.model.biz.LeavesBiz;
import com.zxx.ssm.model.convertor.LeavesConvertor;
import com.zxx.ssm.model.enums.ResultCode;
import com.zxx.ssm.model.form.LeavesForm;
import com.zxx.ssm.model.result.BizResponse;
import com.zxx.ssm.service.EmployeeService;
import com.zxx.ssm.service.LeavesService;
import com.zxx.ssm.util.DateUtils;
import com.zxx.ssm.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by zxx on 2016/12/16.
 */
@RestController
@RequestMapping("personalLeaves")
public class PersonalLeavesController {
    @Autowired
    private LeavesService leavesService;

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public BizResponse getAll(@ModelAttribute LeavesForm leavesForm, @RequestParam(defaultValue = "1") int pageNumber, @RequestParam(defaultValue = "10") int pageSize,
                              @RequestParam(defaultValue = "id") String sort, @RequestParam(defaultValue = "desc") String order, HttpServletRequest request) {
        PaginationUtil.preset(pageNumber, pageSize, sort, order);
        HttpSession session = request.getSession();
        Employee curEmployee = (Employee) session.getAttribute("user");
        leavesForm.setEmployeeId(curEmployee.getId());
        leavesForm.setStartTime(DateUtils.convertString(leavesForm.getStartTime()));
        leavesForm.setEndTime(DateUtils.convertString(leavesForm.getEndTime()));
        List<LeavesBiz> leavesBizList = leavesService.getAll(leavesForm);
        PageInfo pageInfo = new PageInfo(leavesBizList);
        BizResponse bizResponse = new BizResponse();
        bizResponse.put("leaves", pageInfo);
        return bizResponse;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse add(@ModelAttribute LeavesForm leavesForm, HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            Employee curEmployee = (Employee) session.getAttribute("user");
            int leftDay = leavesService.getLeftDay(curEmployee.getId(), leavesForm.getType());
            leavesForm.setEmployeeId(curEmployee.getId());
            Leaves leaves = LeavesConvertor.convertLeavesForm(leavesForm);
            int days = (int) ((leaves.getEndTime().getTime() - leaves.getStartTime().getTime()) / (1000 * 60 * 60 * 24));
            if(leftDay < days)
                return new BizResponse(ResultCode.LEAVES_DAY_NOT_ENOUGH);
            else {
                leavesService.insertSelective(leaves);
                return new BizResponse();
            }
        }catch (Exception e){
            return new BizResponse(ResultCode.LEAVES_TYPE_ADD_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public BizResponse update(@ModelAttribute LeavesForm leavesForm, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Employee curEmployee = (Employee) session.getAttribute("user");
        int leftDay = leavesService.getLeftDay(curEmployee.getId(), leavesForm.getType());
        leavesForm.setEmployeeId(curEmployee.getId());
        Leaves leaves = LeavesConvertor.convertLeavesForm(leavesForm);
        int days = (int) ((leaves.getEndTime().getTime() - leaves.getStartTime().getTime()) / (1000 * 60 * 60 * 24));
        if(leftDay < days){
            return new BizResponse(ResultCode.LEAVES_DAY_NOT_ENOUGH);
        }else {
            leaves.setState(1);
            LeavesExample leavesExample = new LeavesExample();
            leavesExample.createCriteria().andIdEqualTo(leaves.getId());
            if (leavesService.updateByExample(leaves, leavesExample) > 0)
                return new BizResponse();
            else
                return new BizResponse(ResultCode.LEAVES_TYPE_UPDATE_ERROR);
        }
    }

    @RequestMapping(value = "/{strid}", method = RequestMethod.DELETE)
    @ResponseBody
    public BizResponse remove( @PathVariable("strid") String strid) throws AbstractServiceRuntimeException {
        if (strid != null) {
            String[] idList = strid.split(",");
            for (int i = 0; i < idList.length; i++) {
                try {
                    Integer id = Integer.parseInt(idList[i]);
                    if (id != null)
                        leavesService.delete(id);
                } catch (AbstractServiceRuntimeException e) {
                    e.printStackTrace();
                    return new BizResponse(ResultCode.LEAVES_TYPE_DELETE_ERROR);
                }
            }
        }
        return new BizResponse();
    }
}
