package com.zxx.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.zxx.ssm.dao.Employee;
import com.zxx.ssm.dao.EmployeeExample;
import com.zxx.ssm.exception.AbstractServiceRuntimeException;
import com.zxx.ssm.model.biz.EmployeeBiz;
import com.zxx.ssm.model.convertor.EmployeeConvertor;
import com.zxx.ssm.model.enums.ResultCode;
import com.zxx.ssm.model.form.EmployeeForm;
import com.zxx.ssm.model.result.BizResponse;
import com.zxx.ssm.service.EmployeeService;
import com.zxx.ssm.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by zxx on 2016/11/19.
 */
@RestController
@RequestMapping("employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value="",method = RequestMethod.GET)
    @ResponseBody
    public BizResponse getEmployeeList(@ModelAttribute EmployeeForm employeeForm, @RequestParam(defaultValue = "1") int pageNumber, @RequestParam(defaultValue = "1") int pageSize,
                                       @RequestParam(defaultValue = "id") String sort, @RequestParam(defaultValue = "desc") String order) {
        PaginationUtil.preset(pageNumber, pageSize, sort, order);
        List<EmployeeBiz> employeeBizList = employeeService.getAll(employeeForm);
        PageInfo pageInfo = new PageInfo(employeeBizList);
        BizResponse bizResponse = new BizResponse();
        bizResponse.put("employee", pageInfo);
        return bizResponse;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse addEmployee(@ModelAttribute EmployeeForm employeeForm) {
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andCodeEqualTo(employeeForm.getCode());
        List<Employee> list = employeeService.selectByExample(employeeExample);
        BizResponse bizResponse;
        if (list.size() > 0) {
            bizResponse = new BizResponse(ResultCode.EMPLOYEE_TYPE_REGISTER_ERROR);
            bizResponse.put("message", "此工号已经注册过");
            return bizResponse;
        } else {
            if (employeeForm.getLevel() != 1) {
                EmployeeExample tmpExample = new EmployeeExample();
                EmployeeExample.Criteria tmpCriteria = tmpExample.createCriteria();
                tmpCriteria.andLevelEqualTo(employeeForm.getLevel());
                if (employeeForm.getDepartmentId() != null)
                    tmpCriteria.andDepartmentIdEqualTo(employeeForm.getDepartmentId());
                List<Employee> employeeList = employeeService.selectByExample(tmpExample);
                if (employeeList.size() > 0) {
                    Employee tmpEmployee = employeeList.get(0);
                    tmpEmployee.setLevel(1);
                    employeeService.updateByPrimaryKeySelective(tmpEmployee);
                }
            }
            Employee employee = EmployeeConvertor.convertToEmployee(employeeForm);
            if (employeeService.insertSelective(employee) > 0)
                return new BizResponse();
            else
                return new BizResponse(ResultCode.EMPLOYEE_TYPE_ADD_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public BizResponse updateEmployee(@ModelAttribute EmployeeForm employeeForm) {
        if (employeeForm.getLevel()!=null&&employeeForm.getLevel() != 1) {
            EmployeeExample tmpExample = new EmployeeExample();
            EmployeeExample.Criteria tmpCriteria = tmpExample.createCriteria();
            tmpCriteria.andLevelEqualTo(employeeForm.getLevel());
            if (employeeForm.getDepartmentId() != null)
                tmpCriteria.andDepartmentIdEqualTo(employeeForm.getDepartmentId());
            tmpCriteria.andIdNotEqualTo(employeeForm.getId());
            List<Employee> employeeList = employeeService.selectByExample(tmpExample);
            if (employeeList.size() > 0) {
                Employee tmpEmployee = employeeList.get(0);
                tmpEmployee.setLevel(1);
                employeeService.updateByPrimaryKeySelective(tmpEmployee);
            }
        }
        Employee employee = EmployeeConvertor.convertToEmployee(employeeForm);
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andIdEqualTo(employee.getId());
        if (employeeService.updateByExample(employee, employeeExample) > 0)
            return new BizResponse();
        else
            return new BizResponse(ResultCode.EMPLOYEE_TYPE_UPDATE_ERROR);
    }

    @RequestMapping(value = "/{strid}", method = RequestMethod.DELETE)
    @ResponseBody
    public BizResponse removeEmployee(@PathVariable("strid") String strid) {
        if (strid != null) {
            String[] idList = strid.split(",");
            for (int i = 0; i < idList.length; i++) {
                try {
                    Integer id = Integer.parseInt(idList[i]);
                    if (id != null)
                        employeeService.delete(id);
                } catch (AbstractServiceRuntimeException e) {
                    e.printStackTrace();
                    return new BizResponse(ResultCode.EMPLOYEE_TYPE_DELETE_ERROR);
                }
            }
        }
        return new BizResponse();
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse login(String code, String password, HttpServletRequest request) {
        EmployeeExample employeeExample = new EmployeeExample();
        EmployeeExample.Criteria criteria = employeeExample.createCriteria();
        criteria.andCodeEqualTo(code);
        criteria.andPasswordEqualTo(password);
        List<Employee> employeeList = employeeService.selectByExample(employeeExample);
        if (employeeList.size() > 0) {
            HttpSession session = request.getSession();
            session.setAttribute("user", employeeList.get(0));
            return new BizResponse();
        } else
            return new BizResponse(ResultCode.EMPLOYEE_TYPE_LOGIN_ERROR);
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    @ResponseBody
    public BizResponse logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("user", null);
        return new BizResponse();
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse register(@ModelAttribute EmployeeForm employeeForm) {
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andCodeEqualTo(employeeForm.getCode());
        List<Employee> employeeList = employeeService.selectByExample(employeeExample);
        BizResponse bizResponse;
        if (employeeList.size() > 0) {
            bizResponse = new BizResponse(ResultCode.EMPLOYEE_TYPE_REGISTER_ERROR);
            bizResponse.put("message", "此工号已经注册过");
            return bizResponse;

        } else {
            Employee employee = EmployeeConvertor.convertToEmployee(employeeForm);
            if (employeeService.insertSelective(employee) > 0)
                return new BizResponse();
            else
                return new BizResponse(ResultCode.EMPLOYEE_TYPE_REGISTER_ERROR);
        }
    }

    @RequestMapping(value = "checkSession", method = RequestMethod.GET)
    @ResponseBody
    public BizResponse checkSession(HttpServletRequest request){
        HttpSession session =request.getSession();
        if(session.getAttribute("user")==null)
            return new BizResponse(ResultCode.NULL_ARGUMENT);
        else
            return new BizResponse();
    }

    @RequestMapping(value = "getUserInfo", method = RequestMethod.GET)
    @ResponseBody
    public BizResponse getUserInfo(HttpServletRequest request){
        HttpSession session =request.getSession();
        Employee employee = (Employee) session.getAttribute("user");
        if(employee==null)
            return new BizResponse(ResultCode.NULL_ARGUMENT);
        else{
            EmployeeForm employeeForm = new EmployeeForm();
            employeeForm.setId(employee.getId());
            List<EmployeeBiz> employeeBizList = employeeService.getAll(employeeForm);
            BizResponse bizResponse = new BizResponse();
            bizResponse.put("user", employeeBizList.get(0));
            return bizResponse;
        }
    }

    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse updateUser(@ModelAttribute EmployeeForm employeeForm, HttpServletRequest request){
        HttpSession session = request.getSession();
        Employee employee = EmployeeConvertor.convertToEmployee(employeeForm);
        if (employeeService.updateByPrimaryKeySelective(employee) > 0){
            Employee employee1 = employeeService.selectByPrimaryKey(employee.getId());
            session.setAttribute("user", employee1);
            return new BizResponse();
        }
        else
            return new BizResponse(ResultCode.EMPLOYEE_TYPE_UPDATE_ERROR);
    }
}
