package com.zxx.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.zxx.ssm.dao.*;
import com.zxx.ssm.exception.AbstractServiceRuntimeException;
import com.zxx.ssm.model.biz.FieldBiz;
import com.zxx.ssm.model.convertor.FieldConvertor;
import com.zxx.ssm.model.convertor.LeavesConvertor;
import com.zxx.ssm.model.enums.ResultCode;
import com.zxx.ssm.model.form.FieldForm;
import com.zxx.ssm.model.result.BizResponse;
import com.zxx.ssm.service.EmployeeService;
import com.zxx.ssm.service.FieldService;
import com.zxx.ssm.util.DateUtils;
import com.zxx.ssm.util.PaginationUtil;
import com.zxx.ssm.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by bwju on 2016/12/8.
 */
@RestController
@RequestMapping("checkfields")
public class CheckFieldController {
    @Autowired
    private FieldService fieldService;

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public BizResponse getAll(@ModelAttribute FieldForm fieldForm, @RequestParam(defaultValue = "1") int pageNumber, @RequestParam(defaultValue = "1") int pageSize,
                              @RequestParam(defaultValue = "id") String sort, @RequestParam(defaultValue = "desc") String order, HttpServletRequest request) {
        PaginationUtil.preset(pageNumber, pageSize, sort, order);
        fieldForm.setStartTime(DateUtils.convertString(fieldForm.getStartTime()));
        fieldForm.setEndTime(DateUtils.convertString(fieldForm.getEndTime()));
        HttpSession session = request.getSession();
        Employee curEmployee = (Employee) session.getAttribute("user");
        if(curEmployee.getLevel() == 2){
            fieldForm.setCurUserDepartmentId(curEmployee.getDepartmentId());
        }
        List<FieldBiz> fieldsBizList = fieldService.getAllFromCurUserDepartment(fieldForm);
        PageInfo pageInfo = new PageInfo(fieldsBizList);
        BizResponse bizResponse = new BizResponse();
        bizResponse.put("fields", pageInfo);
        return bizResponse;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public BizResponse add(@ModelAttribute FieldForm fieldForm) {
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andCodeEqualTo(fieldForm.getEmployeeCode());
        List<Employee> employeeList = employeeService.selectByExample(employeeExample);
        try {
            if (employeeList.size() > 0) {
                fieldForm.setEmployeeId(employeeList.get(0).getId());
                Field theadd = FieldConvertor.convertFieldForm(fieldForm);
                fieldService.insertSelective(theadd);
                return new BizResponse();
            } else{
                return new BizResponse(ResultCode.EMPLOYEE_TYPE_NOT_EXIST);
            }
        }catch (Exception e){
            return new BizResponse(false,ResultCode.FIELD_TYPE_ADD_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public BizResponse remove( @PathVariable("id") String id) throws AbstractServiceRuntimeException {
        try {
            if (id != null) {
                fieldService.delete(StringUtils.toInt(id));
                return new BizResponse();
            }
            return new BizResponse(false,ResultCode.FIELD_TYPE_DELETE_ERROR);
        }catch (Exception e){
            return new BizResponse(false,ResultCode.FIELD_TYPE_DELETE_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public BizResponse update(@ModelAttribute FieldForm fieldForm) {
        EmployeeExample employeeExample = new EmployeeExample();
        employeeExample.createCriteria().andCodeEqualTo(fieldForm.getEmployeeCode());
        List<Employee> employeeList = employeeService.selectByExample(employeeExample);
        if(employeeList.size() == 0){
            return new BizResponse(ResultCode.EMPLOYEE_TYPE_NOT_EXIST);
        }
        fieldForm.setEmployeeId(employeeList.get(0).getId());
        Field field = FieldConvertor.convertFieldForm(fieldForm);
        FieldExample fieldExample = new FieldExample();
        fieldExample.createCriteria().andIdEqualTo(field.getId());
        if (fieldService.updateByExample(field, fieldExample) > 0)
            return new BizResponse();
        else
            return new BizResponse(ResultCode.FIELD_TYPE_UPDATE_ERROR);
    }
}
