package com.zxx.ssm.auth;

import org.apache.shiro.authz.AuthorizationException;
import org.omg.CORBA.Object;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by zxx on 2016/12/9.
 */
public class SecurityInterceptor implements HandlerInterceptor {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, java.lang.Object o) throws IOException {
        // intercept
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("user") == null) {
                throw new AuthorizationException();
            } else {
                return true;
            }
        }catch (AuthorizationException e){
            response.sendRedirect("/home/login");
        }
        return false;
    }

    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, java.lang.Object o, ModelAndView modelAndView) throws Exception {
    }

    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, java.lang.Object o, Exception e) throws Exception {

    }
}
