package com.zxx.ssm.model.result;

import com.zxx.ssm.model.enums.ResultCode;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by zxx on 2016/11/27.
 */
public class BizResponse extends HashMap implements Serializable {
    private static final long serialVersionUID = -645660450630516270L;

    public BizResponse(){
        this(true, ResultCode.SUCCESS);
    }

    public BizResponse(ResultCode resultCode){
        this(resultCode == ResultCode.SUCCESS, resultCode);
    }

    public BizResponse(boolean success, ResultCode resultCode){
        this.put("success", success);
        this.put("resultCode", resultCode);
        this.put("message", resultCode.getMessage());
    }

    @Override
    public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
