package com.zxx.ssm.model.biz;

import com.zxx.ssm.dao.Leaves;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * Created by zxx on 2016/12/7.
 */
public class LeavesBiz extends Leaves {
    private String employeeCode;

    private String employeeName;

    private String typeName;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
