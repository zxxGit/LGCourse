package com.zxx.ssm.model.biz;

import com.zxx.ssm.dao.Signinout;

/**
 * Created by OPTIPLEX on 2016/12/18.
 */

public class SigninoutSummaryBiz extends Signinout {

    private String employeeName;
    private int signin_count;
    private int leaves_count;
    private int field_count;

    public int getLeaves_count() {
        return leaves_count;
    }

    public void setLeaves_count(int leaves_count) {
        this.leaves_count = leaves_count;
    }

    public int getSignin_count() {
        return signin_count;
    }

    public void setSignin_count(int signin_count) {
        this.signin_count = signin_count;
    }

    public int getField_count() {
        return field_count;
    }

    public void setField_count(int field_count) {
        this.field_count = field_count;
    }


    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }


}