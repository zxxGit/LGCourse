package com.zxx.ssm.model.biz;

import com.zxx.ssm.dao.Field;

/**
 * Created by bwju on 2016/12/8.
 */
public class FieldBiz extends Field{
    String employeeName;

    String employeeCode;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
}
