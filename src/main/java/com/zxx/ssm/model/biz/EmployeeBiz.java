package com.zxx.ssm.model.biz;

import com.zxx.ssm.dao.Employee;

/**
 * Created by zxx on 2016/12/5.
 */
public class EmployeeBiz extends Employee {
    private String departmentName;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
