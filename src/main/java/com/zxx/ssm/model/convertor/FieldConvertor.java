package com.zxx.ssm.model.convertor;

import com.zxx.ssm.dao.Field;
import com.zxx.ssm.model.form.FieldForm;
import com.zxx.ssm.util.DateUtils;

/**
 * Created by bwju on 2016/12/9.
 */
public class FieldConvertor {
    public static Field convertFieldForm(FieldForm FieldForm){
        Field Field = new Field();
        Field.setEmployeeId(FieldForm.getEmployeeId());
        Field.setStartTime(DateUtils.toDate(FieldForm.getStartTime()));
        Field.setEndTime(DateUtils.toDate(FieldForm.getEndTime()));
        Field.setId(FieldForm.getId());
        Field.setReason(FieldForm.getReason());
        Field.setReply(FieldForm.getReply());
        Field.setState(FieldForm.getState());
        return Field;
    }
}
