package com.zxx.ssm.model.convertor;

import com.zxx.ssm.dao.Leaves;
import com.zxx.ssm.model.form.LeavesForm;
import com.zxx.ssm.util.DateUtils;

import java.util.Date;

/**
 * Created by zxx on 2016/12/7.
 */
public class LeavesConvertor {
    public static Leaves convertLeavesForm(LeavesForm leavesForm){
        Leaves leaves = new Leaves();
        leaves.setEmployeeId(leavesForm.getEmployeeId());
        leaves.setStartTime(DateUtils.toDate(leavesForm.getStartTime()));
        leaves.setEndTime(DateUtils.toDate(leavesForm.getEndTime()));
        leaves.setId(leavesForm.getId());
        leaves.setReason(leavesForm.getReason());
        leaves.setType(leavesForm.getType());
        leaves.setReply(leavesForm.getReply());
        leaves.setState(leavesForm.getState());
        return leaves;
    }
}
