package com.zxx.ssm.model.convertor;

import com.zxx.ssm.dao.Employee;
import com.zxx.ssm.model.form.EmployeeForm;

/**
 * Created by zxx on 2016/12/5.
 */
public class EmployeeConvertor {
    public static Employee convertToEmployee(EmployeeForm employeeForm){
        Employee employee = new Employee();
        employee.setId(employeeForm.getId());
        employee.setCode(employeeForm.getCode());
        employee.setSex(employeeForm.getSex());
        employee.setName(employeeForm.getName());
        employee.setPassword(employeeForm.getPassword());
        employee.setDepartmentId(employeeForm.getDepartmentId());
        employee.setLevel(employeeForm.getLevel());
        employee.setEmail(employeeForm.getEmail());
        return employee;
    }
}
