package com.zxx.ssm.model.convertor;
import com.zxx.ssm.dao.Signinout;
import com.zxx.ssm.model.form.SigninoutForm;
import com.zxx.ssm.util.DateUtils;
/**
 * Created by OPTIPLEX on 2016/12/11.
 */
public class SigninoutConvertor {
    public static Signinout convertToSigninout(SigninoutForm SigninoutForm) {

        Signinout Signinout = new Signinout();
        Signinout.setEmployeeCode(SigninoutForm.getEmployeeCode());
        if (SigninoutForm.getStartTime()==null | SigninoutForm.getStartTime() ==""){
            Signinout.setStartTime(null);
        }
        else {
            Signinout.setStartTime(DateUtils.toDateTime(SigninoutForm.getDate() + " " + SigninoutForm.getStartTime()));
        }
        if (SigninoutForm.getEndTime()==null | SigninoutForm.getEndTime() ==""){
            Signinout.setEndTime(null);
        }
        else {
            Signinout.setEndTime(DateUtils.toDateTime(SigninoutForm.getDate() + " " + SigninoutForm.getEndTime()));
        }
        Signinout.setDate(DateUtils.toDate(SigninoutForm.getDate()));
        Signinout.setId(SigninoutForm.getId());
        return Signinout;
    }


}

