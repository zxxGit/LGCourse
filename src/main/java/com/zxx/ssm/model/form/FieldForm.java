package com.zxx.ssm.model.form;

/**
 * Created by bwju on 2016/12/8.
 */
public class FieldForm {
    private Integer id;

    private Integer employeeId;

    private String employeeName;

    private String employeeCode;

    private String startTime;

    private String endTime;

    private Integer state;

    private String reason;

    private String reply;

    private Integer curUserDepartmentId;

    public Integer getCurUserDepartmentId() {
        return curUserDepartmentId;
    }

    public void setCurUserDepartmentId(Integer curUserDepartmentId) {
        this.curUserDepartmentId = curUserDepartmentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply == null ? null : reply.trim();
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
}
