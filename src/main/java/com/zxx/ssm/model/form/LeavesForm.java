package com.zxx.ssm.model.form;

/**
 * Created by zxx on 2016/12/7.
 */
public class LeavesForm {
    private Integer id;

    private Integer employeeId;

    private String employeeCode;

    private String employeeName;

    private String startTime;

    private String endTime;

    private Integer type;

    private Integer state;

    private String reason;

    private String reply;

    private Integer curUserDepartmentId;

    public Integer getCurUserDepartmentId() {
        return curUserDepartmentId;
    }

    public void setCurUserDepartmentId(Integer curUserDepartmentId) {
        this.curUserDepartmentId = curUserDepartmentId;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }
}
