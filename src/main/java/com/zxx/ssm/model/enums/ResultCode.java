package com.zxx.ssm.model.enums;

/**
 * Created by zxx on 2016/11/27.
 */
public enum ResultCode {
    //basic code
    SUCCESS("SUCCESS", "操作成功"),
    SYSTEM_FAILURE("SYSTEM_FAILURE", "系统错误"),
    NOT_SUPPORT("NOT_SUPPORT", "系统不支持该功能"),
    NULL_ARGUMENT("NULL_ARGUMENT", "参数为空"),
    ILLEGAL_ARGUMENT("ILLEGAL_ARGUMENT", "参数不正确"),
    IO_FAILURE("IO_FAILURE", "IO错误"),
    PARTIAL_FAILURE("PARTIAL_FAILURE", "部分失败"),

    //员工业务
    EMPLOYEE_PROCESS_ERROR("EMPLOYEE_PROCESS_ERROR", "员工业务操作失败"),
    EMPLOYEE_CODE_REPEAT_ERROR("EMPLOYEE_CODE_REPEAT_ERROR", "员工工号已存在"),
    EMPLOYEE_TYPE_ADD_ERROR("EMPLOYEE_TYPE_ADD_ERROR", "添加员工操作失败"),
    EMPLOYEE_TYPE_DELETE_ERROR("EMPLOYEE_TYPE_DELETE_ERROR", "删除员工操作失败"),
    EMPLOYEE_TYPE_NOT_EXIST("EMPLOYEE_TYPE_NOT_EXIST", "该员工不存在"),
    EMPLOYEE_TYPE_UPDATE_ERROR("EMPLOYEE_TYPE_UPDATE_ERROR", "更新员工操作失败"),
    EMPLOYEE_TYPE_LOGIN_ERROR("EMPLOYEE_TYPE_LOGIN_ERROR", "账号或密码错误"),
    EMPLOYEE_TYPE_REGISTER_ERROR("EMPLOYEE_TYPE_REGISTER_ERROR", "注册失败"),

    //打卡业务
    SIGNINOUT_PROCESS_ERROR("SIGNINOUT_PROCESS_ERROR", "打卡业务操作失败"),

    //请假业务
    LEAVES_PROCESS_ERROR("LEAVES_PROCESS_ERROR", "请假业务操作失败"),
    LEAVES_TYPE_ADD_ERROR("LEAVES_TYPE_ADD_ERROR", "添加请假记录失败"),
    LEAVES_TYPE_UPDATE_ERROR("LEAVES_TYPE_UPDATE_ERROR","更新请假记录失败"),
    LEAVES_TYPE_DELETE_ERROR("LEAVES_TYPE_DELETE_ERROR", "删除请假记录失败"),
    LEAVES_DAY_NOT_ENOUGH("LEAVES_DAY_NOT_ENOUGH","剩余请假天数不足"),
    //部门业务
    DEPARTMENT_PROCESS_ERROR("DEPARTMENT_PROCESS_ERROR", "部门业务操作失败"),
    DEPARTMENT_TYPE_ADD_ERROR("DEPARTMENT_TYPE_ADD_ERROR", "添加部门失败"),
    DEPARTMENT_TYPE_UPDATE_ERROR("DEPARTMENT_TYPE_UPDATE_ERROR","更新部门失败"),
    DEPARTMENT_TYPE_DELETE_ERROR("DEPARTMENT_TYPE_DELETE_ERROR", "删除部门失败,该部门正在使用"),
    DEPARTMENT_ALREADY_EXIST("DEPARTMENT_ALREADY_EXIST","该部门已存在"),
    //外出申请业务
    FIELD_PROCESS_ERROR("FIELD_PROCESS_ERROR", "外出申请业务操作失败"),
    FIELD_TYPE_ADD_ERROR("FIELD_TYPE_ADD_ERROR", "添加外出申请记录失败"),
    FIELD_TYPE_UPDATE_ERROR("FIELD_TYPE_UPDATE_ERROR","更新外出申请记录失败"),
    FIELD_TYPE_DELETE_ERROR("FIELD_TYPE_DELETE_ERROR", "删除外出申请记录失败");

    private String value;
    private String message;

    ResultCode(String value, String message) {
        this.value = value;
        this.message = message;
    }

    /**
     * 根据枚举值获取枚举对象，如果找不到对应的枚举返回null
     *
     * @param value
     * @return
     */
    public static ResultCode getEnumByValue(String value) {
        for (ResultCode resultCode : ResultCode.values()) {
            if (resultCode.getValue().equals(value))
                return resultCode;
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }
}
