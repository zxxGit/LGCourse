package com.zxx.ssm.mapper;

import com.zxx.ssm.dao.Signinout;
import com.zxx.ssm.dao.SigninoutExample;
import com.zxx.ssm.dao.core.BaseMapper;
import com.zxx.ssm.model.biz.SigninoutBiz;
import com.zxx.ssm.model.form.SigninoutForm;

import java.util.List;

public interface SigninoutMapper extends BaseMapper<Signinout, SigninoutExample, Integer> {
    List<SigninoutBiz> getAll(SigninoutForm signinoutForm);
}