package com.zxx.ssm.mapper;

import com.zxx.ssm.dao.Holiday;
import com.zxx.ssm.dao.HolidayExample;
import java.util.List;

import com.zxx.ssm.dao.core.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface HolidayMapper extends BaseMapper<Holiday, HolidayExample, Integer> {

}