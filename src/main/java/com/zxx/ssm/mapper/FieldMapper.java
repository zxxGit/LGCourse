package com.zxx.ssm.mapper;

import com.zxx.ssm.dao.Field;
import com.zxx.ssm.dao.FieldExample;
import java.util.List;

import com.zxx.ssm.dao.core.BaseMapper;
import com.zxx.ssm.model.biz.FieldBiz;
import com.zxx.ssm.model.form.FieldForm;
import org.apache.ibatis.annotations.Param;

public interface FieldMapper extends BaseMapper<Field, FieldExample, Integer> {
    List<FieldBiz> getAll(FieldForm fieldForm);
    List<FieldBiz> getAllFromCurUserDepartment(FieldForm fieldForm);

    List<FieldBiz> getTodayField(String curDate);
}