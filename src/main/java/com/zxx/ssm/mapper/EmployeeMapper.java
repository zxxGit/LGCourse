package com.zxx.ssm.mapper;

import com.zxx.ssm.dao.Employee;
import com.zxx.ssm.dao.EmployeeExample;

import java.util.List;

import com.zxx.ssm.dao.core.BaseMapper;
import com.zxx.ssm.model.biz.EmployeeBiz;
import com.zxx.ssm.model.form.EmployeeForm;
import org.apache.ibatis.annotations.Param;

public interface EmployeeMapper extends BaseMapper<Employee, EmployeeExample, Integer>{

    List<EmployeeBiz> getAll(EmployeeForm employeeForm);
}