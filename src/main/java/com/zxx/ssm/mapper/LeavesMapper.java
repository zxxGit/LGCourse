package com.zxx.ssm.mapper;

import com.zxx.ssm.dao.Leaves;
import com.zxx.ssm.dao.LeavesExample;
import java.util.List;

import com.zxx.ssm.dao.core.BaseMapper;
import com.zxx.ssm.model.biz.LeavesBiz;
import com.zxx.ssm.model.form.LeavesForm;

public interface LeavesMapper extends BaseMapper<Leaves, LeavesExample, Integer> {
    List<LeavesBiz> getAll(LeavesForm leavesForm);

    List<LeavesBiz> getAllFromCurUserDepartment(LeavesForm leavesForm);

    List<LeavesBiz> getTodayLeaves(String curDate);
}