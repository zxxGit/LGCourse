package com.zxx.ssm.mapper;

import com.zxx.ssm.dao.Department;
import com.zxx.ssm.dao.DepartmentExample;
import java.util.List;

import com.zxx.ssm.dao.core.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface DepartmentMapper extends BaseMapper<Department, DepartmentExample, Integer>{
}