package com.zxx.ssm.util;

import com.github.pagehelper.PageHelper;

/**
 * Created by zxx on 2016/11/27.
 */
public class PaginationUtil {
    public static void preset(int page, int rows, String sort, String order) {
        if (sort != null && order != null) {
            PageHelper.startPage(page, rows, sort + " " + order);
        } else {
            PageHelper.startPage(page, rows);
        }
    }
}
