package com.zxx.ssm.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 类DateUtils.java的实现描述： 日期工具类
 *
 * @author JungleKing
 */
public class DateUtils {

    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
    public static final String DEFAULT_YEAR_MONTH_PATTERN = "yyyy-MM";
    public static final String DEFAULT_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_DATEDETAIL_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String MONTH7_PATTERN = "yyyy-MM";
    public static final String DATE8_PATTERN = "yyyyMMdd";
    public static final String DATE10_PATTERN = "yyyy-MM-dd";
    public static final String DATE5_PATTERN = "MM-dd";
    public static final String TIME6_PATTERN = "HHmmss";
    public static final String TIME8_PATTERN = "HH:mm:ss";
    public static final String DATETIME14_PATTERN = "yyyyMMddHHmmss";
    public static final String DATETIME19_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String DATEMSEL18_PATTERN = "yyyyMMddHHmmssSSSS";
    public static final String DATE16_PATTERN = "yyyy-MM-dd HH:mm";
    public static final String DEFAULT_DATE7_PATTERN = "yyyy-MM";
    public static final String DEFAULT_DATE6PATTERN = "yyyyMM";
    public static final String DEFAULT_DATE_PATTERN_CHINESE = "yyyy年MM月dd日";

    /**
     * 生成一个日期转换格式
     *
     * @param pattern 日期转换格式
     * @return
     */
    private static SimpleDateFormat getDateParser(String pattern) {
        return new SimpleDateFormat(pattern);
    }

    /**
     * 获取当前日期
     *
     * @return
     */
    public static Date curDate() {
        return new Date();
    }

    /**
     * 按照指定格式转换当前日期，返回转换后的字符串
     *
     * @param strFormat 转换格式
     * @return
     */
    public static String curDateStr(String strFormat) {
        Date date = new Date();
        return getDateParser(strFormat).format(date);
    }

    /**
     * 返回默认的日期字符串yyyy-MM-dd，形式如： 2008-02-29
     */
    public static String curDateStr() {
        Date date = new Date();
        return getDateParser(DEFAULT_DATE_PATTERN).format(date);
    }

    /**
     * 获取当前日期对应的Timestamp类型数据
     *
     * @return
     */
    public static Timestamp curTimestamp() {
        return new Timestamp(new Date().getTime());
    }

    /**
     * 将字符串转换为日期型的数据，如：DateUtils.toDate("2008-03-22","yyyy-MM-dd");如果格式不匹配就返回为null
     *
     * @param dateString 指定字符串
     * @param pattern    转换格式
     * @return
     */
    public static Date toDate(String dateString, String pattern) {
        Date date = null;
        try {
            date = getDateParser(pattern).parse(dateString);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 将指定字符串按照默认的转换格式(yyyy-MM-dd)转换为日期型
     *
     * @param dateString
     * @return
     */
    public static Date toDate(String dateString) {
        Date date = null;
        try {
            date = getDateParser(DEFAULT_DATE_PATTERN).parse(dateString);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 按照19位的转换格式(yyyy-MM-dd HH:mm:ss)转换字符串为日期型
     *
     * @param dateString
     * @return
     */
    public static Date toDateTime(String dateString) {
        Date date = null;
        try {
            date = getDateParser(DATETIME19_PATTERN).parse(dateString);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 将日期按照指定格式转换为字符串
     *
     * @param date    指定日期
     * @param pattern 转换格式
     * @return
     */
    public static String toDateStr(Date date, String pattern) {
        if (date == null || pattern == null) {
            return null;
        }
        return getDateParser(pattern).format(date);
    }

    /**
     * 将日期按照默认格式(yyyy-MM-dd)进行转换
     *
     * @param date
     * @return
     */
    public static String toDateStr(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DEFAULT_DATE_PATTERN).format(date);
    }

    /**
     * 按照yyyy-MM-dd HH:mm:ss格式转换日期
     *
     * @param date
     * @return
     */
    public static String toDateTimeStr(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DATETIME19_PATTERN).format(date);
    }

    /**
     * @param nowTime
     * @param timeType
     * @param step
     * @return
     */
    public static Date addCalendar(Calendar nowTime, int timeType, int step) {
        try {
            nowTime.add(timeType, step);
        } catch (Exception e) {
            return null;
        }
        return nowTime.getTime();
    }

    /**
     * @return
     */
    public static Calendar curCalendar() {
        Calendar calendar = null;
        try {
            calendar = Calendar.getInstance();
        } catch (Exception e) {
            return null;
        }
        return calendar;
    }
public static String convertString(String strDate){
    if(StringUtils.isNotBlank(strDate)){
        strDate = strDate.replace("GMT", "").replaceAll("\\(.*\\)", "");
        //将字符串转化为date类型，格式2016-10-12
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss z",Locale.ENGLISH);
        Date dateTrans = null;
        try {
            dateTrans = format.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(dateTrans);
    }else
        return null;

}


    /**
     * 将当前日期转换为yyyyMMdd格式
     *
     * @return
     */
    public static String curDateStr8() {
        Date date = new Date();
        return getDateParser(DATE8_PATTERN).format(date);
    }

    /**
     * 将当前日期转换为yyyy-MM-dd格式
     *
     * @return
     */
    public static String curDateStr10() {
        Date date = new Date();
        return getDateParser(DEFAULT_DATE_PATTERN).format(date);
    }

    /**
     * 将当前日期转换为yyyyMMddHHmmss格式
     *
     * @return
     */
    public static String curDateTimeStr14() {
        Date date = new Date();
        return getDateParser(DATETIME14_PATTERN).format(date);
    }

    /**
     * 将当前日期转换为(yyyy-MM-dd HHmmss)格式
     *
     * @return
     */
    public static String curDateTimeStr19() {
        Date date = new Date();
        return getDateParser(DATETIME19_PATTERN).format(date);
    }

    /**
     * 转换为时间格式HHmmss
     *
     * @return
     */
    public static String curTimeStr6() {
        Date date = new Date();
        return getDateParser(TIME6_PATTERN).format(date);
    }

    /**
     * 转换为yyyyMMddHHmmssSSSS格式
     *
     * @return
     */
    public static String curDateMselStr18() {
        Date date = new Date();
        return getDateParser(DATEMSEL18_PATTERN).format(date);
    }

    /**
     * 将指定字符串按照默认的yyyyMMdd
     *
     * @param dateStr
     * @return
     */
    public static Date toDate8(String dateStr) {
        Date date = null;
        try {
            date = getDateParser(DATE8_PATTERN).parse(dateStr);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 按照yyyy-MM-dd格式将指定字符串转换为日期
     *
     * @param dateStr
     * @return
     */
    public static Date toDate10(String dateStr) {
        Date date = null;
        try {
            date = getDateParser(DEFAULT_DATE_PATTERN).parse(dateStr);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 按照yyyyMMddHHmmss将指定字符串转换为日期
     *
     * @param dateStr
     * @return
     */
    public static Date toDateTime14(String dateStr) {
        Date date = null;
        try {
            date = getDateParser(DATETIME14_PATTERN).parse(dateStr);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 按照yyyy-MM-dd HH:mm:ss将指定字符串转换为日期
     *
     * @param dateStr
     * @return
     */
    public static Date toDateTime19(String dateStr) {
        Date date = null;
        try {
            date = getDateParser(DATETIME19_PATTERN).parse(dateStr);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 将指定日期按照格式yyyyMMdd转换为字符串
     *
     * @param date
     * @return
     */
    public static String toDateStr8(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DEFAULT_DATE_PATTERN).format(date);
    }

    /**
     * 将指定日期按照格式yyyy-MM转换为字符串
     *
     * @param date
     * @return
     */
    public static String toDateStr6(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DEFAULT_DATE6PATTERN).format(date);
    }

    /**
     * 将指定日期按照格式yyyy-MM转换为字符串
     *
     * @param date
     * @return
     */
    public static String toDateStr7(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DEFAULT_DATE7_PATTERN).format(date);
    }

    /**
     * 以 yyyy-MM-dd HH:mm格式返回
     */
    public static String toDateStr16(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DATE16_PATTERN).format(date);
    }

    /**
     * 将指定日期按照格式yyyy-MM-dd转换为字符串
     *
     * @param date
     * @return
     */
    public static String toDateStr10(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DATE10_PATTERN).format(date);
    }

    /**
     * 将指定日期按照格式yyyyMMddHHmmss转换为字符串
     *
     * @param date
     * @return
     */
    public static String toDateTimeStr14(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DATETIME14_PATTERN).format(date);
    }

    /**
     * 将指定日期按照格式yyyy-MM-dd HH:mm:ss转换为字符串
     *
     * @param date
     * @return
     */
    public static String toDateTimeStr19(Date date) {
        if (date == null) {
            return "";
        }
        return getDateParser(DATETIME19_PATTERN).format(date);
    }

    /**
     * 在date增加天数，返回新的日期，如果days为负数，那么就是返回之前的日期。如DateUtils.addDays(new Date(),2);就是得到2天后的日期
     *
     * @param date
     * @param days
     * @return
     */
    public static Date addDays(Date date, int days) {
        if (date == null) {
            return null;
        }
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    /**
     * 月份增加，返回增加之后的日期，month为负数即表示之前的月份
     *
     * @param date
     * @param month
     * @return
     */
    public static Date addMonth(Date date, int month) {
        if (date == null) {
            return null;
        }
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, month);
        return calendar.getTime();
    }

    /**
     * 判断date是否在date1和date2之间，允许相等
     *
     * @param date
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isDateBetween(Date date, Date date1, Date date2) {
        return ((date1.before(date) || date1.equals(date)) && (date.before(date2) || date.equals(date2)));
    }

    /**
     * 返回2个日期相差的天数
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int getDaysInterval(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null) {
            return 0;
        }
        long timeInterval = toDate.getTime() - fromDate.getTime();
        int daysInterval = (int) (timeInterval / 86400000L);
        return daysInterval;
    }

    /**
     * @param fromTime 起始时间，一般来说，会小于toTime
     * @param toTime   结束时间
     * @return 结束时间-起始时间
     */
    public static int getSecondsInterval(Date fromTime, Date toTime) {
        if (fromTime == null || toTime == null) {
            return 0;
        }
        long timeInterval = toTime.getTime() - fromTime.getTime();
        int daysInterval = (int) (timeInterval / 1000L);
        return daysInterval;
    }

    /**
     * 返回当前日期是当年的第几周，每周从星期一开始计算
     *
     * @param date
     * @return
     */
    public static int getWeekOfYear(Date date) {
        if (date == null) {
            return -1;
        }
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        int week = calendar.get(3);
        return week;
    }

    /**
     * 判断指定日期是星期几
     *
     * @param date
     * @return
     */
    public static int getDayOfWeek(Date date) {
        if (date == null) {
            return -1;
        }
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int day = calendar.get(7);
        day--;
        if (0 == day) day = 7;
        return day;
    }

    /**
     * 返回日期所在周的第一天（周一到周日算一整周）
     *
     * @param date
     * @return
     */
    public static Date getStarOfWeek(Date date) {
        int dayIndex = getDayOfWeek(date);
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(GregorianCalendar.DATE, -dayIndex + 1);// 前推N天的日期
        return calendar.getTime();
    }

    /**
     * 返回对应的中文星期
     *
     * @param date
     * @return
     */
    public static String getCnDayOfWeek(Date date) {
        int i = getDayOfWeek(date);
        String str = "";
        switch (i) {
            case 1:
                str = "星期一";
                break;
            case 2:
                str = "星期二";
                break;
            case 3:
                str = "星期三";
                break;
            case 4:
                str = "星期四";
                break;
            case 5:
                str = "星期五";
                break;
            case 6:
                str = "星期六";
                break;
            case 7:
                str = "星期日";
                break;
            default:
                str = "未知错误";
                break;
        }
        return str;
    }


    /**
     * 返回对应的中文星期
     *
     * @param date
     * @return
     */
    public static String getCnDayNumOfWeek(Date date) {
        int i = getDayOfWeek(date);
        String str = "";
        switch (i) {
            case 1:
                str = "一";
                break;
            case 2:
                str = "二";
                break;
            case 3:
                str = "三";
                break;
            case 4:
                str = "四";
                break;
            case 5:
                str = "五";
                break;
            case 6:
                str = "六";
                break;
            case 7:
                str = "日";
                break;
            default:
                str = "未知错误";
                break;
        }
        return str;
    }

    /**
     * 返回对应的英文星期
     *
     * @param date
     * @return
     */
    public static String getEnDayOfWeek(Date date) {
        int i = getDayOfWeek(date);
        String str = "";
        switch (i) {
            case 1:
                str = "Monday";
                break;
            case 2:
                str = "Tuesday";
                break;
            case 3:
                str = "Wednesday";
                break;
            case 4:
                str = "Thursday";
                break;
            case 5:
                str = "Friday";
                break;
            case 6:
                str = "Saturday";
                break;
            case 7:
                str = "Sunday";
                break;
            default:
                str = "Unknow Error";
                break;
        }
        return str;
    }

    /**
     * 返回指定日期所在月份的最后一天，例如是传入2008年2月2日，那么就会返回2008年2月29日
     *
     * @param date
     * @return
     */
    public static Date getLastDayInMonth(Date date) {
        return getLastDayInMonth(date, 0);
    }

    /**
     * 获取指定日期所在年的第一天以及最后一天
     *
     * @param date
     * @return
     */
    public static Map<String, Date> getStartToEnd(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        Map<String, Date> map = new HashMap<String, Date>();
        map.put("startTime", toDate10(year + "-01-01"));
        map.put("endTime", toDate10(year + "-12-31"));
        return map;
    }

    /**
     * 返回指定日期下一个月的最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDayInNextMonth(Date date) {
        return getLastDayInMonth(date, 1);
    }

    /**
     * 返回指定日期i个月后的那个月的最后一天的日期
     *
     * @param date
     * @param i
     * @return
     */
    public static Date getLastDayInMonth(Date date, int i) {
        if (date == null) {
            return null;
        }
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, i + 1);
        calendar.set(5, 1);
        calendar.add(5, -1);
        return calendar.getTime();
    }

    public static String between(long interval) {
        long days = interval / (3600 * 1000 * 24);
        interval = interval % (3600 * 1000 * 24);
        long hours = interval / (3600 * 1000);
        interval = interval % (3600 * 1000);
        long minutes = interval / (60 * 1000);
        interval = interval % (60 * 1000);
        long seconds = interval / (1000);
        interval = interval % (1000);
        return days + "天" + hours + "小时" + minutes + "分钟" + seconds + "秒" + interval + "毫秒";
    }

    /**
     * 日期装换函数格式2011-08-01 yyyy-MM-dd
     *
     * @param str
     * @return
     */
    public static Date strToDate(String str) {
        if (str == null) {
            return null;
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            Date date = dateFormat.parse(str);
            return date;
        } catch (ParseException e) {
            // logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * 根据日期，获取最近的6个月 [数据原因暂时改为5个月]
     *
     * @param latestMonth
     * @return
     */
    public static List<Map<String, String>> getLatest6Month(Date latestMonth) {
        if (latestMonth == null) {
            throw new IllegalArgumentException("the month can not be null while computing latest 6 month");
        }
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        for (Date date = addMonth(latestMonth, -4); date.compareTo(latestMonth) <= 0; date = addMonth(date, 1)) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("yyyymmdd", toDateStr(date, "yyyyMMdd"));
            map.put("year", toDateStr(date, "yyyy"));
            map.put("month", toDateStr(date, "MM"));
            list.add(map);
        }
        return list;
    }

    /**
     * 根据日期获取最近的两个季度 [数据原因暂时改为一季度]
     *
     * @param latestQuarter
     * @return
     */
    public static List<Map<String, String>> getLatest2Quarter(Date latestQuarter) {
        if (latestQuarter == null) {
            throw new IllegalArgumentException("the quarter can not be null while computing latest 2 quarter");
        }
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        // for (Date date = DateUtils.addMonth(latestQuarter, -3); date.compareTo(latestQuarter) <= 0; date = DateUtils
        // .addMonth(date, 3)) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("yyyymmdd", toDateStr(latestQuarter, "yyyyMMdd"));
        map.put("year", toDateStr(latestQuarter, "yyyy"));
        map.put("quarter", String.valueOf(DateUtils.getQuarter(latestQuarter)));
        list.add(map);
        // }
        return list;
    }

    /**
     * 根据日期，获取该日期所在的季度
     *
     * @param date
     * @return 返回1-4，分别表示四个季度
     */
    public static int getQuarter(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("date can not be null in computing the quarter");
        }
        int month = Integer.valueOf(toDateStr(date, "MM"));
        if (month > 6) {
            if (month >= 10) {
                return 4;
            }
            return 3;
        } else {
            if (month <= 3) {
                return 1;
            }
            return 2;
        }
    }

    /**
     * 拼接两个日期，生成一个跨度字符串
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static String getDatePeriodStringByStartAndEndDate(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            throw new IllegalArgumentException("date can not be null in composing date period");
        }
        if (startDate.compareTo(endDate) > 0) {
            throw new IllegalArgumentException("start date must before end date");
        }
        if (startDate.compareTo(endDate) == 0) {
            return toDateStr(startDate, "yyyy.MM.dd");
        }
        return toDateStr(startDate, "yyyy.MM.dd") + " - " + toDateStr(endDate, "yyyy.MM.dd");
    }

    /**
     * 拼接两个日期，生成一个用括号括起来的跨度字符串(左括号左边带空格)
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static String getDatePeriodStringByStartAndEndDateWithBracket(Date startDate, Date endDate) {
        String datePeriod = getDatePeriodStringByStartAndEndDate(startDate, endDate);
        return " (" + datePeriod + ")";
    }

    /**
     * 根据开始和结束日期，生成一个包含从开始日期到结束日期所有日期的列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<String> getDateListByStartAndEndDate(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            throw new IllegalArgumentException("date can not be null while generating date list");
        }
        if (startDate.compareTo(endDate) > 0) {
            throw new IllegalArgumentException("start date must before end date");
        }
        List<String> list = new ArrayList<String>();
        Date date = startDate;
        while (date.compareTo(endDate) <= 0) {
            list.add(toDateStr(date, "MM-dd"));
            date = addDays(date, 1);
        }
        return list;
    }

    /**
     * 根据开始和结束日期，生成一个包含从开始日期到结束日期所有日期的列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<String> getDateListEndAndStar(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            throw new IllegalArgumentException("date can not be null while generating date list");
        }
        if (startDate.compareTo(endDate) > 0) {
            throw new IllegalArgumentException("start date must before end date");
        }
        List<String> list = new ArrayList<String>();
        Date date = startDate;
        while (date.compareTo(endDate) <= 0) {
            list.add(toDateStr(date, DATE10_PATTERN));
            date = addDays(date, 1);
        }
        return list;
    }

    /**
     * 根据开始和结束日期，生成一个包含从开始日期到结束日期所有日期的列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<String> getDateListByStartAndEndDateDyn(Date startDate, Date endDate, String pattern) {
        if (startDate == null || endDate == null) {
            throw new IllegalArgumentException("date can not be null while generating date list");
        }
        if (startDate.compareTo(endDate) > 0) {
            throw new IllegalArgumentException("start date must before end date");
        }
        List<String> list = new ArrayList<String>();
        Date date = startDate;
        while (date.compareTo(endDate) <= 0) {
            list.add(toDateStr(date, pattern));
            date = addDays(date, 1);
        }
        return list;
    }

    public static Date accurate2month(Date month) {
        Calendar c = Calendar.getInstance();
        c.setTime(month);
        c.set(Calendar.DATE, 1);
        accurate2day(c);
        return c.getTime();
    }

    public static Date accurate2day(Date day) {
        Calendar c = Calendar.getInstance();
        c.setTime(day);
        accurate2day(c);
        return c.getTime();
    }

    /**
     * 精确到天
     *
     * @param c
     */
    private static void accurate2day(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
    }

    /**
     * 获取当年指定月份的第一天日期
     *
     * @param month
     * @return
     */
    public static String getFirstDayOfMonth(int month) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(DateUtils.curDate());
        calendar.set(GregorianCalendar.MONTH, month - 1);
        calendar.set(GregorianCalendar.DAY_OF_MONTH, 1);
        return getDateParser(DEFAULT_DATE_PATTERN).format(calendar.getTime());
    }

    /**
     * 计算两天之间的间隔<br>
     *
     * @param bigDay   大的那个日期。只取到天，会忽略时、分、秒
     * @param smallDay 小的那个日期。只取到天，会忽略时、分、秒
     * @return 返回两天之间的天数间隔。如果日期的大小颠倒，则返回负值
     */
    public static int getDateInterval(Date bigDay, Date smallDay) {
        Date big = accurate2day(bigDay);
        Date small = accurate2day(smallDay);
        long diff = big.getTime() - small.getTime();
        return (int) (diff / (1000 * 60 * 60 * 24));
    }

    /**
     * 获取页面下拉的13个特定月份<br>
     * 从month指定的月份起，倒推12个月份，总共13个。
     *
     * @param month 从通信表里面取到的month数据
     * @return
     */
    public static List<String> get13Months(Date month) {
        List<String> l = new ArrayList<String>();

        if (month == null) {
            return l;
        }

        Date lastMonth = accurate2month(month);
        for (int i = 0; i < 13; i++) {
            Date m = addMonth(lastMonth, -1 * i);
            l.add(toDateStr(m, MONTH7_PATTERN));
        }
        return l;
    }

    /**
     * 获取指定日期月份最近的12个月份（不包含当前月份）
     *
     * @param month
     * @return
     */
    public static List<String> getLast12Months(Date month) {
        List<String> l = new ArrayList<String>();

        if (month == null) {
            return l;
        }

        Date lastMonth = accurate2month(month);
        for (int i = 1; i < 13; i++) {
            Date m = addMonth(lastMonth, -1 * i);
            l.add(toDateStr(m, MONTH7_PATTERN));
        }
        return l;
    }

    public static Date copyDate(Date date) {
        if (date == null) {
            return null;
        }

        return new Date(date.getTime());
    }

    public static int getDayCountOfMonth(Date month) {
        if (month == null) {
            throw new IllegalArgumentException("month can not be null");
        }
        Calendar c = Calendar.getInstance();
        c.setTime(getLastDayInMonth(month));
        return c.get(Calendar.DATE);
    }

    public static boolean timeEquals(Date time1, Date time2) {
        if (time1 == null && time2 == null) {
            return true;
        }
        if (time1 == null || time2 == null) {
            return false;
        }
        return time1.getTime() == time2.getTime();
    }

    public static String getTodayDateStr() {
        return DateUtils.toDateStr(new Date());
    }

    public static List<Date> getDayDates(int amount) {
        List<Date> dates = new ArrayList<Date>();
        Date day = accurate2day(new Date());
        for (int i = amount - 1; i >= 0; i--) {
            dates.add(addDays(day, -1 * i));
        }
        return dates;
    }

    public static Date timeStamp2Date(Timestamp ts) {
        if (ts == null) {
            return null;
        }
        return new Date(ts.getTime());
    }
}
