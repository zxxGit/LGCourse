package com.zxx.ssm.service;

import com.zxx.ssm.dao.core.BaseMapper;
import com.zxx.ssm.exception.AbstractServiceRuntimeException;
import com.zxx.ssm.service.core.IBaseService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by zxx on 2016/11/27.
 */
abstract class AbstractService<E, C, ID, T extends BaseMapper<E, C, ID>> implements IBaseService<E, C, ID> {

    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    protected T baseMapper;

    public AbstractService(T baseMapper) {
        this.baseMapper = baseMapper;
    }

    public int countByExample(C example) {
        return baseMapper.countByExample(example);
    }

    public int deleteByExample(C example) {
        return baseMapper.deleteByExample(example);
    }

    public int delete(ID id) throws AbstractServiceRuntimeException {
        int affectRows = 0;
        try {
            affectRows = baseMapper.deleteByPrimaryKey(id);
        } catch (Exception ex) {
            throw new AbstractServiceRuntimeException(ex);
        }
        return affectRows;
    }

    public int insert(E record) {
        return baseMapper.insert(record);
    }

    public int insertSelective(E record) {
        return baseMapper.insertSelective(record);
    }

    public List<E> selectByExample(C example) {
        return baseMapper.selectByExample(example);
    }

    public E selectByPrimaryKey(ID id) {
        return baseMapper.selectByPrimaryKey(id);
    }

    public int updateByExampleSelective(@Param("record") E record, @Param("example") C example) {
        return baseMapper.updateByExampleSelective(record, example);
    }

    public int updateByExample(@Param("record") E record, @Param("example") C example) {
        return baseMapper.updateByExample(record, example);
    }

    public int updateByPrimaryKeySelective(E record) {
        return baseMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(E record) {
        return baseMapper.updateByPrimaryKey(record);
    }
}
