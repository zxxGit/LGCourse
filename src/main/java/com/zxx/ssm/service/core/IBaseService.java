package com.zxx.ssm.service.core;

import com.zxx.ssm.exception.AbstractServiceRuntimeException;

import java.util.List;

/**
 * Created by zxx on 2016/11/27.
 */
public interface IBaseService<E, C, ID> {
    int countByExample(C example);

    int deleteByExample(C example);

    int delete(ID id) throws AbstractServiceRuntimeException;

    int insert(E record);

    int insertSelective(E record);

    List<E> selectByExample(C example);

    E selectByPrimaryKey(ID id);

    int updateByExampleSelective(E record, C example);

    int updateByExample(E record, C example);

    int updateByPrimaryKeySelective(E record);

    int updateByPrimaryKey(E record);
}