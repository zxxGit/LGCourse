package com.zxx.ssm.service;

import com.zxx.ssm.dao.Employee;
import com.zxx.ssm.dao.EmployeeExample;
import com.zxx.ssm.mapper.EmployeeMapper;
import com.zxx.ssm.model.biz.EmployeeBiz;
import com.zxx.ssm.model.form.EmployeeForm;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zxx on 2016/11/19.
 */
@Service
public class EmployeeService extends AbstractService<Employee, EmployeeExample, Integer, EmployeeMapper> implements IEmployeeService{

    public EmployeeService(EmployeeMapper employeeMapper){super(employeeMapper);}

    public List<EmployeeBiz> getAll(EmployeeForm employeeForm) {
        List<EmployeeBiz> employeeBizList = this.baseMapper.getAll(employeeForm);
        return employeeBizList;
    }
}
