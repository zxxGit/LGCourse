package com.zxx.ssm.service;


import com.zxx.ssm.dao.Signinout;
import com.zxx.ssm.dao.SigninoutExample;
import com.zxx.ssm.mapper.SigninoutMapper;
import com.zxx.ssm.model.biz.SigninoutBiz;
import com.zxx.ssm.model.form.SigninoutForm;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by OPTIPLEX on 2016/12/11.
 */

@Service
public class SigninoutService extends AbstractService< Signinout,SigninoutExample,  Integer, SigninoutMapper> implements ISigninoutService{

    public SigninoutService(SigninoutMapper signinoutMapper){super(signinoutMapper);}

    public List<SigninoutBiz> getAll(SigninoutForm signinoutForm) {
        List<SigninoutBiz> signinoutBizList = this.baseMapper.getAll(signinoutForm);
        return signinoutBizList;
    }

}