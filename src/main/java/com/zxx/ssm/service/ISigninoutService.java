package com.zxx.ssm.service;


import com.zxx.ssm.dao.Signinout;
import com.zxx.ssm.dao.SigninoutExample;
import com.zxx.ssm.model.biz.SigninoutBiz;
import com.zxx.ssm.model.form.SigninoutForm;
import com.zxx.ssm.service.core.IBaseService;

import java.util.List;

/**
 * Created by OPTIPLEX on 2016/12/11.
 */

public interface ISigninoutService extends IBaseService<Signinout, SigninoutExample, Integer> {

    List<SigninoutBiz> getAll(SigninoutForm signinoutForm);


}
