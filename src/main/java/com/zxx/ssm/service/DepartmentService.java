package com.zxx.ssm.service;

import com.zxx.ssm.dao.Department;
import com.zxx.ssm.dao.DepartmentExample;
import com.zxx.ssm.mapper.DepartmentMapper;
import org.springframework.stereotype.Service;

/**
 * Created by zxx on 2016/12/8.
 */
@Service
public class DepartmentService extends AbstractService<Department, DepartmentExample, Integer, DepartmentMapper> implements IDepartmentService {
    public DepartmentService(DepartmentMapper departmentMapper){super(departmentMapper);}
}
