package com.zxx.ssm.service;

import com.zxx.ssm.dao.Holiday;
import com.zxx.ssm.dao.Leaves;
import com.zxx.ssm.dao.LeavesExample;
import com.zxx.ssm.mapper.HolidayMapper;
import com.zxx.ssm.mapper.LeavesMapper;
import com.zxx.ssm.model.biz.LeavesBiz;
import com.zxx.ssm.model.form.LeavesForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zxx on 2016/12/6.
 */
@Service
public class LeavesService extends AbstractService<Leaves, LeavesExample, Integer, LeavesMapper> implements ILeavesService {
    public LeavesService(LeavesMapper leavesMapper) {
        super(leavesMapper);
    }

    @Autowired
    private HolidayMapper holidayMapper;

    public List<LeavesBiz> getAll(LeavesForm leavesForm) {
        List<LeavesBiz> leavesBizList = this.baseMapper.getAll(leavesForm);
        return leavesBizList;
    }

    public int getLeftDay(Integer id, Integer type) {
        int day = 0;
        LeavesExample leavesExample = new LeavesExample();
        LeavesExample.Criteria criteria = leavesExample.createCriteria();
        criteria.andEmployeeIdEqualTo(id);
        criteria.andTypeEqualTo(type);
        List<Leaves> leavesList = this.baseMapper.selectByExample(leavesExample);
        for (int i = 0; i < leavesList.size(); i++) {
            day += (leavesList.get(i).getEndTime().getTime() - leavesList.get(i).getStartTime().getTime()) / (1000 * 60 * 60 * 24);
        }
        Holiday holiday = holidayMapper.selectByPrimaryKey(type);
        day = holiday.getDays() - day;
        return day;
    }

    public int getLeftDay2(Integer id, Integer id1, Integer type) {
        int day = 0;
        LeavesExample leavesExample = new LeavesExample();
        LeavesExample.Criteria criteria = leavesExample.createCriteria();
        criteria.andIdNotEqualTo(id);
        criteria.andEmployeeIdEqualTo(id1);
        criteria.andTypeEqualTo(type);
        List<Leaves> leavesList = this.baseMapper.selectByExample(leavesExample);
        for (int i = 0; i < leavesList.size(); i++) {
            day += (leavesList.get(i).getEndTime().getTime() - leavesList.get(i).getStartTime().getTime()) / (1000 * 60 * 60 * 24);
        }
        Holiday holiday = holidayMapper.selectByPrimaryKey(type);
        day = holiday.getDays() - day;
        return day;
    }

    public List<LeavesBiz> getAllFromCurUserDepartment(LeavesForm leavesForm) {
        List<LeavesBiz> list = this.baseMapper.getAllFromCurUserDepartment(leavesForm);
        return list;
    }

    public List<LeavesBiz> getTodayLeaves(String date) {
        List<LeavesBiz> leavesBizList= this.baseMapper.getTodayLeaves(date);
        return leavesBizList;
    }
}
