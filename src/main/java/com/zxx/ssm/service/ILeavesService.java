package com.zxx.ssm.service;

import com.zxx.ssm.dao.Leaves;
import com.zxx.ssm.dao.LeavesExample;
import com.zxx.ssm.model.biz.LeavesBiz;
import com.zxx.ssm.model.form.LeavesForm;
import com.zxx.ssm.service.core.IBaseService;

import java.util.List;

/**
 * Created by zxx on 2016/12/6.
 */
public interface ILeavesService extends IBaseService<Leaves, LeavesExample, Integer> {
    List<LeavesBiz> getAll(LeavesForm leavesForm);

    int getLeftDay(Integer id, Integer type);

    int getLeftDay2(Integer id, Integer id1, Integer type);

    List<LeavesBiz> getAllFromCurUserDepartment(LeavesForm leavesForm);

    List<LeavesBiz> getTodayLeaves(String date);
}
