package com.zxx.ssm.service;

import com.zxx.ssm.dao.Employee;
import com.zxx.ssm.dao.EmployeeExample;
import com.zxx.ssm.model.biz.EmployeeBiz;
import com.zxx.ssm.model.form.EmployeeForm;
import com.zxx.ssm.service.core.IBaseService;

import java.util.List;

/**
 * Created by zxx on 2016/11/27.
 */
public interface IEmployeeService extends IBaseService<Employee, EmployeeExample, Integer> {

    List<EmployeeBiz> getAll(EmployeeForm employeeForm);


}
