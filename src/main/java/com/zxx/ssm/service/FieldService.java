package com.zxx.ssm.service;

import com.zxx.ssm.dao.Field;
import com.zxx.ssm.dao.FieldExample;
import com.zxx.ssm.mapper.FieldMapper;
import com.zxx.ssm.model.biz.FieldBiz;
import com.zxx.ssm.model.form.FieldForm;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by bwju on 2016/12/8.
 */
@Service
public class FieldService extends AbstractService<Field, FieldExample, Integer, FieldMapper> implements IFieldService {

    public FieldService(FieldMapper baseMapper) {
        super(baseMapper);
    }

    public List<FieldBiz> getAll(FieldForm fieldForm) {
        List<FieldBiz> list = this.baseMapper.getAll(fieldForm);
        return list;
    }

    public List<FieldBiz> getAllFromCurUserDepartment(FieldForm fieldForm){
        List<FieldBiz> list = this.baseMapper.getAllFromCurUserDepartment(fieldForm);
        return list;
    }

    public List<FieldBiz> getTodayField(String date) {
        List<FieldBiz> fieldBizList= this.baseMapper.getTodayField(date);
        return fieldBizList;
    }
}
