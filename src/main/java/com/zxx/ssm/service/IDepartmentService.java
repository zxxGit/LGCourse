package com.zxx.ssm.service;

import com.zxx.ssm.dao.Department;
import com.zxx.ssm.dao.DepartmentExample;
import com.zxx.ssm.service.core.IBaseService;

/**
 * Created by zxx on 2016/12/8.
 */
public interface IDepartmentService extends IBaseService<Department, DepartmentExample, Integer>{
}
