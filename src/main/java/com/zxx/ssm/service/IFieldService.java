package com.zxx.ssm.service;

import com.zxx.ssm.dao.Field;
import com.zxx.ssm.dao.FieldExample;
import com.zxx.ssm.model.biz.FieldBiz;
import com.zxx.ssm.model.form.FieldForm;
import com.zxx.ssm.service.core.IBaseService;

import java.util.List;

/**
 * Created by bwju on 2016/12/8.
 */
public interface IFieldService extends IBaseService<Field, FieldExample, Integer> {
    List<FieldBiz> getAll(FieldForm fieldForm);
    List<FieldBiz> getAllFromCurUserDepartment(FieldForm fieldForm);

    List<FieldBiz> getTodayField(String date);
}
