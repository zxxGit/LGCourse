package com.zxx.ssm.exception;

/**
 * Created by zxx on 2016/11/27.
 */
public class AbstractServiceRuntimeException extends Exception {
    public AbstractServiceRuntimeException(Exception ex) {
        super(ex);
    }
}
