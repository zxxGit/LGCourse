package com.zxx.ssm.dao.core;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by zxx on 2016/11/27.
 */
public interface BaseMapper<E, C, ID> {
    int countByExample(C example);

    int deleteByExample(C example);

    int deleteByPrimaryKey(ID id);

    int insert(E record);

    int insertSelective(E record);

    List<E> selectByExample(C example);

    E selectByPrimaryKey(ID id);

    int updateByExampleSelective(@Param("record") E record, @Param("example") C example);

    int updateByExample(@Param("record") E record, @Param("example") C example);

    int updateByPrimaryKeySelective(E record);

    int updateByPrimaryKey(E record);
}
