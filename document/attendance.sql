/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : attendance

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2016-12-11 14:17:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '行政部11');
INSERT INTO `department` VALUES ('2', '财务部');
INSERT INTO `department` VALUES ('3', '开发部');
INSERT INTO `department` VALUES ('4', '测试部');
INSERT INTO `department` VALUES ('5', '运维部');
INSERT INTO `department` VALUES ('6', '公关部');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL COMMENT '0是女，1是男',
  `code` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL COMMENT '0是管理员，1普通员工，2部门经理，3总经理',
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk1` (`department_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('9', 'hl', '1', '2323', '2223323', '1', '1', '1289312140@qq.com');
INSERT INTO `employee` VALUES ('10', '233', '1', '123', '1231232', '1', '1', '1289312140@qq');
INSERT INTO `employee` VALUES ('11', '111', '1', '111', '1111233', '1', '1', '123@q.com');
INSERT INTO `employee` VALUES ('13', 'zxx1232', '1', '119', '111111', '1', '1', '2016218018@tju.edu.cn');

-- ----------------------------
-- Table structure for field
-- ----------------------------
DROP TABLE IF EXISTS `field`;
CREATE TABLE `field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `state` int(11) DEFAULT '10' COMMENT '10待审，20部门经理审过，30通过，40被拒',
  `reason` varchar(255) DEFAULT NULL,
  `reply` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk2` (`employee_id`),
  CONSTRAINT `fk2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of field
-- ----------------------------

-- ----------------------------
-- Table structure for holiday
-- ----------------------------
DROP TABLE IF EXISTS `holiday`;
CREATE TABLE `holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of holiday
-- ----------------------------
INSERT INTO `holiday` VALUES ('1', '病假', '10');
INSERT INTO `holiday` VALUES ('2', '事假', '20');
INSERT INTO `holiday` VALUES ('3', '年假', '30');

-- ----------------------------
-- Table structure for leaves
-- ----------------------------
DROP TABLE IF EXISTS `leaves`;
CREATE TABLE `leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1病假，2事假，3年假，4婚假，5产假，6陪产假',
  `state` int(11) DEFAULT '1' COMMENT '1待审，2部门经理审过，3通过，4被拒',
  `reason` varchar(255) DEFAULT NULL,
  `reply` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk3` (`employee_id`) USING BTREE,
  CONSTRAINT `fk3` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leaves
-- ----------------------------
INSERT INTO `leaves` VALUES ('2', '11', '2016-12-07 00:00:00', '2016-12-15 00:00:00', '1', '1', 'test修改', '');
INSERT INTO `leaves` VALUES ('3', '13', '2016-12-08 00:00:00', '2016-12-09 00:00:00', '1', '1', 'test', '');
INSERT INTO `leaves` VALUES ('4', '13', '2016-12-09 00:00:00', '2016-12-10 00:00:00', '1', '1', 'test', '');

-- ----------------------------
-- Table structure for signinout
-- ----------------------------
DROP TABLE IF EXISTS `signinout`;
CREATE TABLE `signinout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_code` varchar(255) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of signinout
-- ----------------------------
